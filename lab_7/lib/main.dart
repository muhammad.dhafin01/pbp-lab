import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  final _title = TextEditingController();
  final _text = TextEditingController();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Personal Journal",
                 style : TextStyle(color : Colors.red) 
                   ),
      
        backgroundColor : Colors.black
      
      ),
      body: 
      Form(
        
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      controller : _title,
                      style : TextStyle(color : Colors.red), 
                      decoration: new InputDecoration(
                      hintText: "Example : The biggest car",
                      labelText: "Title",
                      labelStyle : TextStyle(
                      color : Colors.red,
                      ),
                      enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.red, width: 0.0),
                      ),
                      
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide:  BorderSide(color: Colors.red ),),
                        ),
                     
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'The title should not be empty';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller : _text,
                    style : TextStyle(color : Colors.red), 
                    maxLines : 15,
                    decoration: new InputDecoration(
                      labelText: "Text",
                      labelStyle : TextStyle(
                      color : Colors.red,
                      ),
                      enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.red, width: 0.0),
                      ),

                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide:  BorderSide(color: Colors.red ),),

                    ),
                    
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'The text should not be empty';
                      }
                      return null;
                    },
                    
                  ),
                 
                ),

                ElevatedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
//                   color: Colors.red,
                  onPressed: () {
                      showDialog(
                      context : context,
                      builder : (context){
                         return AlertDialog(
                         content: Text(_title.text),);
                        },
                      );
                      
                  },
                  style : ElevatedButton.styleFrom(
                         primary : Colors.red,
                            )
                ),
              ],
            ),
          ),
        ),
      ),
      backgroundColor : Colors.black
    );
  }
}