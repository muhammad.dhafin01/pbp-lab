from django.urls import path
from .views import index,add_note,note_list

urlpatterns = [
    path('note_table', index, name='index'),
    path('add_note', add_note, name='add_note'),
    path('note_list',note_list,name='note_list'),
]