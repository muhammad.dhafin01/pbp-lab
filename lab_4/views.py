from django.shortcuts import render,redirect
from lab_2.models import Note
from django.contrib.auth.decorators import login_required
from .forms import NoteForm
from django.http import response



# @login_required(login_url="/admin/login")
def index(request):
    notes = Note.objects.all().values() 
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if(form.is_valid()):
            form.save()
        # if 'next' in request.POST:
        #     return redirect(request.POST.get('next'))
        # else:
            return redirect('index')
    response = {'form':form}
    return render(request,'lab4_form.html',response)

def note_list(request):
    notes = Note.objects.all().values() 
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)


