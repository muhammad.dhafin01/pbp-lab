from django import forms
from django.forms import ModelForm
from lab_2.models import Note



class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ['To','From','Title','message']
    error_messages = {
        'required' : 'Please Type'
    }
    To = forms.CharField(max_length=30)
    From = forms.CharField(max_length=30)
    Title = forms.CharField(max_length=30)
    message = forms.TextInput()