1.Apakah perbedaan antara JSON dan XML?
Perbedaan mendasar dari JSON dan XML ialah JSON merupakan sebuah format data dari JavaScript sedangkan XML merupakan bahasa markup seperti HTML yang lebih menitikberatkan pada struktur dari isi ataupun konten suatu data. Dalam implementasinya, XML digunakan sebagai transportasi data dari satu aplikasi ke aplikasi lain melalui internet, sedangkan JSON digunakan sebagai format pertukaran data ringan yang jauh lebih mudah dipahami oleh para developer karena memiliki struktur kode yang lebih sederhana.

2.Apakah perbedaan antara HTML dan XML?
Perbedaan antara HTML dan XML yaitu HTML lebih menitikberatkan format tampilan suatu data, sedangkan XML lebih menitikberatkan pada struktur dan juga konten ataupun isi dari data tersebut.Ilustrasinya HTML tersusun atas tag-tag yang mengatur tampilan data,tetapi tidak tersedia informasi mengenai konten ataupun isi data-data tersebut.Untuk melengkapi hal tersebut, pada file XML dibuat mengenai konten ataupun isi dari data-data tersebut.Selain itu, informasi mengenai data dan tampilannya akan tersusun secara terpisah apabila dilengkapi dengan file XML tersebut.

Sumber: <br>
https://www.monitorteknologi.com/perbedaan-json-dan-xml/ <br>
https://www.niagahoster.co.id/blog/json-adalah/ <br>
https://id.wikibooks.org/wiki/Pemrograman_XML/XML_vs_HTML <br>
