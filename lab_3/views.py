from django.http import response
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from .forms import FriendForm
from lab_1.models import Friend



@login_required(login_url="/admin/login")
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

def add_friend(request):
    form = FriendForm()
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if(form.is_valid()):
            form.save()
        # if 'next' in request.POST:
        #     return redirect(request.POST.get('next'))
        # else:
            return redirect("/lab-3")
    response = {'form':form}
    return render(request,'lab3_form.html',response)
    