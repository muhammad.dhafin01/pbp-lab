# from django.db import models
from django import forms
from django.forms import ModelForm
from django.forms.widgets import SelectDateWidget
from lab_1.models import Friend


class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name','NPM','DOB']
    error_messages = {
        'required' : 'Please Type'
    }
    name = forms.CharField(max_length=30)
    NPM = forms.CharField(max_length=10)
    TAHUN = range(1945,2021)
    DOB = forms.DateField(widget=forms.SelectDateWidget(years=TAHUN))
