class LayoutConstants {
  static double dimen_1 = 1.0;
  static double dimen_5 = 5.0;
  static double dimen_10 = 10.0;
  static double dimen_15 = 15.0;
  static double dimen_20 = 20.0;
  static double dimen_25 = 25.0;
  static const dimen_50 = 50.0;
  static const dimen_125 = 125.0;
  static const dimen_150 = 150.0;
}
