import 'package:flutter/material.dart';

class ColorConstants {
  static Color primaryColor = Colors.black;
  static const secondaryColor = Color(0xff484848);
}
