import 'package:flutter/material.dart';
import 'package:notes/common/constants/layout_contants.dart';

class TextStyleConstants {
  static TextStyle titleStyle = TextStyle(
    fontSize: LayoutConstants.dimen_25,
    fontWeight: FontWeight.w700,
  );
}
