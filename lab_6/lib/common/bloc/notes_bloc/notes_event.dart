import 'package:notes/domain/entities/note_item_entity.dart';

abstract class NotesEvent {}

class GetNotesEvent extends NotesEvent {}

class UpdateNoteEvent extends NotesEvent {
  final NoteItemEntity entity;

  UpdateNoteEvent({this.entity});
}
