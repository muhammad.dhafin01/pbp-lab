import 'package:flutter/material.dart';
import 'package:notes/presentation/app.dart';

void main() {
  runApp(App());
}

//Nama: Muhammad Dhafin Fauzan
//NPM : 2006596421

//KETERANGAN:
//Kodingan ini saya dapatkan dari referensi repo
//https://gitlab.com/herisetiawan08/notes-app
//yang saya lakukan yaitu memodifikasi dan menyesuaikan sesuai dengan kebutuhan 
//App saya sebelummnya karena beberapa fitur mirip dengan App saya pada website sebelumnya