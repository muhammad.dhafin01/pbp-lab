import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_event.dart';
import 'package:notes/common/constants/routes_constants.dart';
import 'package:notes/presentation/routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NotesBloc>(
          create: (context) => NotesBloc()..add(GetNotesEvent()),
        ),
      ],
      child: MaterialApp(
        routes: Routes.getAll(),
        onGenerateRoute: Routes.generateRotes,
        initialRoute: RouteList.notesList,
        title: 'Add Journal',
      ),
    );
  }
}
