import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_event.dart';
import 'package:notes/common/bloc/notes_bloc/notes_state.dart';
import 'package:notes/common/constants/color_contants.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/common/constants/routes_constants.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_constants.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_screen.dart';
import 'package:notes/presentation/journey/notes_screen/notes_constants.dart';
import 'package:notes/presentation/journey/notes_screen/widgets/notes_item_category.dart';

class NotesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: ColorConstants.primaryColor,
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(LayoutConstants.dimen_10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      NotesLanguage.title,
                      style: TextStyle(
                        fontSize: LayoutConstants.dimen_50,
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    InkWell(
                      onTap: () async {
                        final response = await Navigator.pushNamed(
                          context,
                          RouteList.noteDetail,
                          arguments: NoteDetailArguments(
                            note: NoteItemEntity(
                              createdAt: DateTime.now().toIso8601String(),
                              bgColor: ColorConstants.primaryColor,
                              category: NoteDetailLanguage.defaultCategory1,
                              titleColor: Colors.red,
                            ),
                          ),
                        );
                        if (response is NoteItemEntity) {
                          BlocProvider.of<NotesBloc>(context).add(
                            UpdateNoteEvent(entity: response),
                          );
                        }
                      },
                      child: Icon(
                        Icons.add,
                        color: Colors.red,
                        size: LayoutConstants.dimen_50,
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: BlocBuilder<NotesBloc, NotesState>(
                      builder: (context, state) {
                    return Stack(
                      children: [
                        _mapStateToWidget(context, state),
                        if (state is NotesLoadingState)
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.red,
                                  ),
                                ),
                              ),
                            ],
                          ),
                      ],
                    );
                  }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _mapStateToWidget(BuildContext context, NotesState state) {
    if (state.notes.isEmpty) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(
              bottom: LayoutConstants.dimen_10,
            ),
            height: LayoutConstants.dimen_150,
            width: LayoutConstants.dimen_150,
            decoration: BoxDecoration(
              color: Colors.red,
              shape: BoxShape.circle,
            ),
            child: Icon(
              Icons.close,
              size: LayoutConstants.dimen_125,
              color: ColorConstants.primaryColor,
            ),
          ),
          Text(
            NotesLanguage.emptyText,
            style: TextStyle(
              color: Colors.red,
              fontSize: LayoutConstants.dimen_25,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      );
    }
    final List<String> noteCategories =
        state.notes.map((note) => note.category).toSet().toList();

    return Column(
      children: noteCategories
          .map(
            (category) => NoteItemCategory(
              notes: state.notes
                  .where((note) => note.category == category)
                  .toList(),
            ),
          )
          .toList(),
    );
  }
}
