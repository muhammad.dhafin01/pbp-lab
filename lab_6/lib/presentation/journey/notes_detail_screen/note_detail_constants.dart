import 'package:flutter/material.dart';
import 'package:notes/common/constants/color_contants.dart';
import 'package:notes/domain/entities/color_entity.dart';
import 'package:notes/domain/entities/note_option_entity.dart';

class NoteDetailConstants {
  static List<NoteOptionEntity> threeDotsItem = [
    NoteOptionEntity(
      color: ColorConstants.secondaryColor,
      text: NoteDetailLanguage.editTitle,
      type: NoteOptionEnum.editTitle,
    ),
    NoteOptionEntity(
      color: ColorConstants.secondaryColor,
      text: NoteDetailLanguage.addWidget,
      type: NoteOptionEnum.addWidget,
    ),
    NoteOptionEntity(
      color: ColorConstants.secondaryColor,
      text: NoteDetailLanguage.noteStyle,
      type: NoteOptionEnum.noteStyle,
    ),
    NoteOptionEntity(
      color: Colors.red,
      text: NoteDetailLanguage.deleteNote,
      type: NoteOptionEnum.deleteNote,
    ),
  ];

  static List<ColorEntity> colorSuggestion = [
    ColorEntity(
      name: 'White',
      value: Colors.white,
    ),
    ColorEntity(
      name: 'Black',
      value: Colors.black,
    ),
    ColorEntity(
      name: 'Red',
      value: Colors.red,
    ),
    ColorEntity(
      name: 'Green',
      value: Colors.green,
    ),
    ColorEntity(
      name: 'Blue',
      value: Colors.blue,
    ),
  ];

  static List<ColorEntity> bgColorSuggestion = [
    ColorEntity(
      name: 'Default',
      value: ColorConstants.primaryColor,
    ),
    ColorEntity(
      name: 'Red',
      value: Colors.red,
    ),
  ];

  static List<String> defaultCategories = [
    NoteDetailLanguage.defaultCategory1,
    NoteDetailLanguage.defaultCategory2,
  ];
}

class NoteDetailLanguage {
  static const String editTitle = 'Edit Journal';
  static const String addWidget = 'Add Widget';
  static const String noteStyle = 'Journal Style';
  static const String deleteNote = 'Delete Journal';

  static const String defaultCategory1 = 'Private';
  static const String defaultCategory2 = 'Work';
}
