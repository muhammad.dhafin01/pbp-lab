import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/common/constants/routes_constants.dart';
import 'package:notes/common/constants/text_style_constants.dart';
import 'package:notes/common/navigation/custom_routes.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/domain/entities/note_option_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_bloc.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_event.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_constants.dart';

class NoteDetailHeader extends StatelessWidget {
  final ValueNotifier editMode = ValueNotifier(false);
  final NoteItemEntity note;
  NoteDetailHeader({
    this.note,
  }) {
    editMode.value = note.title == null;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          InkWell(
            onTap: () => Navigator.of(context).pop(note),
            child: Icon(
              Icons.arrow_back_ios,
              color: note.titleColor,
              size: LayoutConstants.dimen_25,
            ),
          ),
          Expanded(
            child: ValueListenableBuilder(
                valueListenable: editMode,
                builder: (context, value, _) {
                  final TextEditingController controller =
                      TextEditingController(
                    text: note.title,
                  );
                  if (value) {
                    return TextField(
                      controller: controller,
                      onSubmitted: (text) {
                        BlocProvider.of<NoteDetailBloc>(context).add(
                          NoteDetailUpdateTitleEvent(title: text),
                        );
                        editMode.value = false;
                      },
                      cursorColor: note.titleColor,
                      style: TextStyleConstants.titleStyle
                          .copyWith(color: note.titleColor),
                      decoration: InputDecoration(
                        hintText: 'Add Journal',
                        border: InputBorder.none,
                      ),
                    );
                  }
                  return Text(
                    note.title,
                    style: TextStyleConstants.titleStyle.copyWith(
                      color: note.titleColor,
                    ),
                  );
                }),
          ),
          PopupMenuButton(
            icon: Icon(
              Icons.more_vert,
              color: note.titleColor,
              size: LayoutConstants.dimen_25,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(LayoutConstants.dimen_10),
            ),
            itemBuilder: (context) => NoteDetailConstants.threeDotsItem
                .map(
                  (item) => PopupMenuItem(
                    value: item.type,
                    child: Text(
                      item.text,
                      style: TextStyle(
                        color: item.color,
                      ),
                    ),
                  ),
                )
                .toList(),
            onSelected: (type) => _menuButtonHandler(type, context),
          ),
        ],
      ),
    );
  }

  void _menuButtonHandler(NoteOptionEnum type, BuildContext context) async {
    switch (type) {
      case NoteOptionEnum.addWidget:
      case NoteOptionEnum.deleteNote:
      case NoteOptionEnum.editTitle:
        editMode.value = true;
        break;
      case NoteOptionEnum.noteStyle:
        final _newNote = await Navigator.pushNamed(
          context,
          CustomRoute.nameWith(
            CustomRoutes.rubber,
            RouteList.noteDetailRubber,
          ),
          arguments: note,
        );
        BlocProvider.of<NoteDetailBloc>(context).add(
          NoteDetailSetEntityEvent(entity: _newNote ?? note),
        );

        break;
      default:
    }
  }
}
