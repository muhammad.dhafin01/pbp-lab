import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/domain/entities/note_content_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_bloc.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_event.dart';
import 'package:notes/presentation/journey/notes_detail_screen/widgets/note_card_widget.dart';

class NotePlainTextWidget extends StatefulWidget {
  final NoteContentPlainEntity content;
  final List<NoteContentEntity> contents;
  NotePlainTextWidget({
    this.content,
    this.contents,
  });
  @override
  _NotePlainTextWidgetState createState() => _NotePlainTextWidgetState();
}

class _NotePlainTextWidgetState extends State<NotePlainTextWidget> {
  ValueNotifier editMode = ValueNotifier(false);
  TextEditingController controller;
  @override
  void initState() {
    super.initState();
    editMode.value = widget.content == null;
    controller = TextEditingController(text: widget.content?.text);
  }

  @override
  Widget build(BuildContext context) {
    return NoteCardWidget(
      child: ValueListenableBuilder(
          valueListenable: editMode,
          builder: (context, value, _) {
            if (value) {
              return TextField(
                maxLines: 99,
                minLines: 1,
                controller: controller,
                onSubmitted: (text) {
                  widget.content.text = text;
                  widget.contents
                    ..removeWhere(
                        (content) => content.key == widget.content.key)
                    ..add(widget.content);
                  BlocProvider.of<NoteDetailBloc>(context).add(
                    NoteDetailUpdateContentsEvent(
                      contents: widget.contents,
                    ),
                  );
                },
                textInputAction: TextInputAction.done,
              );
            }
            return Text(
              widget.content.text,
              textAlign: widget.content.textAlign,
              style: TextStyle(
                fontSize: widget.content.fontSize,
                color: widget.content.textColor,
              ),
            );
          }),
    );
  }
}
