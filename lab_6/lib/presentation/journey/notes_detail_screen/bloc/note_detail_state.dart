import 'package:notes/domain/entities/note_item_entity.dart';

abstract class NoteDetailState {
  final NoteItemEntity entity;
  NoteDetailState({this.entity});
}

class NoteDetailInitState extends NoteDetailState {}

class NoteDetailEditedState extends NoteDetailState {
  NoteDetailEditedState({
    NoteItemEntity entity,
  }) : super(entity: entity);
}
