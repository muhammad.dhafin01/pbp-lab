import 'dart:async';
import 'package:flutter/material.dart';
import 'package:rubber/rubber.dart';

import 'package:notes/presentation/widgets/rubber_widget/rubber_wdget_contstants.dart';

import 'rubber_widget.dart';

class _ModalScopeStatus extends InheritedWidget {
  final RubberModalRoute route;

  _ModalScopeStatus(
    this.route,
    Widget child,
  ) : super(child: child);

  @override
  bool updateShouldNotify(_ModalScopeStatus old) {
    return route.isCurrent;
  }
}

class RubberModalConfirmPopup {
  String title;
  String description;
  VoidCallback onConfirm;
  String confirmButtonText;

  RubberModalConfirmPopup(
    this.title,
    this.description, {
    this.onConfirm,
    this.confirmButtonText,
  });
}

abstract class RubberModalRoute<T> extends OverlayRoute<T> {
  RubberModalRoute({RouteSettings settings}) : super(settings: settings);
  final double offsetHeader = 0;
  final bool dismissible = true;
  final bool split = true;

  RubberModalConfirmPopup requestConfirmClose;

  BuildContext context;

  static void clearAll(BuildContext context) {
    return clearNavigator(Navigator.of(context));
  }

  static void clearNavigator(NavigatorState navigator) {
    while (_routes.isNotEmpty) {
      navigator.removeRoute(_routes.last);
      _routes.removeLast();
    }
    _current = null;
    _currentEntry = null;
  }

  static final List<RubberModalRoute> _routes = [];
  static RubberModalRoute _current;
  static final GlobalKey _currentKey = GlobalKey();
  static OverlayEntry _currentEntry;

  static RubberModalRoute<T> of<T extends Object>(BuildContext context) {
    final _ModalScopeStatus widget =
        context.dependOnInheritedWidgetOfExactType<_ModalScopeStatus>();
    return widget?.route;
  }

  Future<bool> _collapse() async {
    final completer = Completer<bool>();
    final animController = _current?.getAnimationController();

    animController?.collapse()?.whenCompleteOrCancel(() {
      completer.complete(
          animController.animationState.value == AnimationState.collapsed);
    });
    return completer.future;
  }

  RubberAnimationController getAnimationController() {
    final RubberAutoSizeState state = _currentKey.currentState;
    return state.animationController;
  }

  @override
  void install() {
    _routes.add(this);
    super.install();
  }

  @override
  bool get willHandlePopInternally {
    try {
      return getAnimationController().animationState.value !=
              AnimationState.collapsed &&
          _routes.length == 1;
    } catch (_) {
      return false;
    }
  }

  @override
  bool didPop(T result) {
    if (willHandlePopInternally) {
      _collapse().then((value) => value ? navigator?.pop(result) : null);
      return false;
    }
    if (super.didPop(result)) {
      _routes.remove(this);
      if (_routes.isEmpty) {
        _current = null;
        _currentEntry = null;
      }
      return true;
    }
    return false;
  }

  Widget buildHeader(BuildContext context);

  Widget buildContent(BuildContext context, ScrollController controller);

  Widget _buildHeader(BuildContext context) {
    return _ModalScopeStatus(this, Builder(builder: buildHeader));
  }

  Widget _buildContent(BuildContext context, ScrollController controller) {
    return _ModalScopeStatus(
      this,
      Builder(builder: (context) => buildContent(context, controller)),
    );
  }

  void pop() {
    Navigator.of(context).removeRoute(this);
    _routes.removeWhere((route) => route.settings.name == settings.name);
  }

  Future<bool> makeCloseAll([bool force = false]) async {
    if (_current.dismissible == false) {
      return false;
    }
    if (force || _current.requestConfirmClose == null) {
      final FocusScopeNode currentFocus = FocusScope.of(context);
      if (!currentFocus.hasPrimaryFocus) {
        currentFocus.unfocus();
        await Future.delayed(const Duration(
            milliseconds: RubberWidgetConstant.defaultClosingTime));
      }
      FocusScope.of(context).requestFocus(FocusNode());
      return _collapse().then((result) {
        if (result) {
          clearAll(context);
        }
        return result;
      });
    }
    return false;
  }

  @override
  Iterable<OverlayEntry> createOverlayEntries() {
    if (_currentEntry != null) {
      return [OverlayEntry(builder: (_) => Container())];
    }

    return [
      _currentEntry = OverlayEntry(builder: (BuildContext context) {
        if (_routes.isEmpty) {
          return const SizedBox();
        }
        _current = _routes.last..context = context;
        final screenHeight = MediaQuery.of(context).size.height;
        return Material(
          color: const Color(0x00000000),
          child: RubberAutoSize(
            key: _currentKey,
            onClosed: () async {
              if (_routes.isNotEmpty) {
                if (_current.requestConfirmClose != null) {
                  await _current
                      .getAnimationController()
                      .halfExpand(from: 0.00011);
                } else {
                  clearAll(context);
                }
              }
            },
            requestClose: _current.makeCloseAll,
            offsetHeader: _current.offsetHeader,
            split: _current.split,
            header: _current._buildHeader,
            content: _current._buildContent,
            originalHeight: screenHeight,
          ),
        );
      })
    ];
  }
}

typedef RubberModalConfirmPopupBuilder = RubberModalConfirmPopup Function(
    BuildContext);

class RubberModalRouteBuilder<T> extends RubberModalRoute<T> {
  final WidgetBuilder header;
  final ScrollableBuilder content;
  final RubberModalConfirmPopupBuilder requestConfirmCloseBuilder;

  @override
  final double offsetHeader;

  @override
  final bool dismissible;

  @override
  final bool split;

  RubberModalRouteBuilder({
    this.header,
    this.content,
    this.offsetHeader = 0,
    this.dismissible = true,
    this.requestConfirmCloseBuilder,
    BottomSheetThemeData theme,
    RouteSettings settings,
    this.split = true,
  }) : super(
          settings: settings,
        );

  @override
  Widget buildHeader(BuildContext context) {
    return header != null ? header(context) : null;
  }

  @override
  Widget buildContent(BuildContext context, ScrollController controller) {
    return content != null ? content(context, controller) : Container();
  }

  @override
  RubberModalConfirmPopup get requestConfirmClose {
    if (requestConfirmCloseBuilder == null) {
      return super.requestConfirmClose;
    }
    return requestConfirmCloseBuilder(context);
  }
}

Future<T> showModalRubberBottomSheet<T>({
  @required BuildContext context,
  WidgetBuilder header,
  ScrollableBuilder content,
  double offsetHeader = 0,
  bool dismissible = true,
  BottomSheetThemeData theme,
  bool useRootNavigator = false,
  Object arguments,
  bool split = true,
}) {
  return Navigator.of(
    context,
    rootNavigator: useRootNavigator,
  ).push(RubberModalRouteBuilder<T>(
    header: header,
    content: content,
    offsetHeader: offsetHeader,
    dismissible: dismissible,
    theme: theme,
    settings: RouteSettings(arguments: arguments),
    split: split,
  ));
}
