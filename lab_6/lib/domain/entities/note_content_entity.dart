import 'package:flutter/material.dart';
import 'package:notes/domain/entities/task_entity.dart';

abstract class NoteContentEntity {
  final String key;
  final NoteContentType type;

  NoteContentEntity(this.type, {this.key});
}

enum NoteContentType {
  plainText,
  task,
}

class NoteContentPlainEntity extends NoteContentEntity {
  String text;
  Color textColor;
  TextAlign textAlign;
  double fontSize;

  NoteContentPlainEntity(
      {String key, this.text, this.textColor, this.textAlign, this.fontSize})
      : super(NoteContentType.plainText, key: key);
}

class NoteContentTaskEntity extends NoteContentEntity {
  final Color textColor;
  final Color boxColor;
  final List<TaskEntity> taskEntities;

  NoteContentTaskEntity({
    String key,
    this.textColor,
    this.boxColor,
    this.taskEntities,
  }) : super(NoteContentType.task, key: key);
}
